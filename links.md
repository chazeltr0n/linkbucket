##### Universe

http://waitbutwhy.com/2013/08/putting-time-in-perspective.html

http://waitbutwhy.com/2014/05/fermi-paradox.html

http://www-eve.ucdavis.edu/grosberg/Grosberg%20pdf%20papers/2007%20Grosberg%20%26%20Strathmann.AREES.pdf

http://en.wikipedia.org/wiki/Great_Filter

http://en.wikipedia.org/wiki/Space_colonization#Outside_the_Solar_System

http://www.pnas.org/content/110/48/19273.abstract

http://www.seti.org/node/647


##### Elasticsearch

http://www.elasticsearch.org/case-studies/

http://p.brightact.com/p/1412176177151733?e=cliff.hazelton@gmail.com&rep=ElissaNancarrow-NEMEA&mkt_tok=3RkMMJWWfF9wsRogvajLZKXonjHpfsX56%2B0qWqC%2FlMI%2F0ER3fOvrPUfGjI4CTcJrI%2BSLDwEYGJlv6SgFQrHGMa1h17gOUhM%3D

http://www.elasticsearch.org/guide/en/elasticsearch/guide/current/inside-a-shard.html


##### Drones

http://copter.ardupilot.com/


#####  Ruby

https://github.com/styleguide/ruby

http://karmi.cz/en


##### JRuby Deployment

http://recipes.sinatrarb.com/p/deployment/jruby


##### Go

http://www.golang-book.com/6/index.htm

http://www.alexedwards.net/blog/golang-response-snippets

https://golang.org/doc/articles/wiki/

http://blog.golang.org/json-and-go


##### Hadoop

http://hadoop.apache.org/


##### Nginx

http://omgitsmgp.com/2013/09/03/compressing-nginx-json-responses/

##### Ember Cli

http://robots.thoughtbot.com/migrating-from-ember-rails-to-ember-cli

https://github.com/ember-cli/ember-cli


##### Streem

https://github.com/matz/streem/tree/master/examples


##### Guava

https://code.google.com/p/guava-libraries/wiki/ServiceExplained
